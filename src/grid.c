#include "grid.h"
#include "helper.h"

#include <string.h>
#include <stdio.h>
#include <stdbool.h>

#define TEST 1

tBlockID get_cell_block_id(int cell_row, int cell_col)
{
  tBlockID blockID;
  memset(&blockID, '\0', sizeof(tBlockID));
  blockID.block_row = cell_row/BLOCK_SIZE;
  blockID.block_col = cell_col/BLOCK_SIZE;

  return blockID;
}

int initialize_sudoku_grid_from_file(const char *filename, tSudokuGrid *p_sudoku_grid)
{
  // Placeholder for cell values read from file
  int cell_values[GRID_SIZE][GRID_SIZE];
  memset(cell_values, '\0', sizeof(int)*GRID_SIZE*GRID_SIZE);

  // Read cell values from file
  if (get_cell_values_from_file(filename, cell_values) == -1)
  {
    return -1;
  }

  for (int block_row = 0; block_row < BLOCK_SIZE; block_row++)
  {
    for (int block_col = 0; block_col < BLOCK_SIZE; block_col++)
    { 
      memset(p_sudoku_grid->block_info[block_row][block_col].S, true, sizeof(bool)*GRID_SIZE);

      // Iterate over cells in the current block
      int end_row_idx = (block_row+1)*BLOCK_SIZE;
      int end_col_idx = (block_col+1)*BLOCK_SIZE;

      for (int row_cursor = block_row*BLOCK_SIZE; row_cursor < end_row_idx; row_cursor++)
      {
        for (int col_cursor = block_col*BLOCK_SIZE; col_cursor < end_col_idx; col_cursor++)
        {
          // Update candidate number, considering only the numbers already present in the current block
          if (cell_values[row_cursor][col_cursor] != EMPTY)
          {
            p_sudoku_grid->block_info[block_row][block_col].S[cell_values[row_cursor][col_cursor]-1] = false;
          }
        } // cell col
      } // cell row      

    } // block col
  } // block row

  // Now that we have V, H and S set, the next thing to do is to compute A by
  // performing an intersection between V, H and S.
  // Do a second pass through the block
  for (int block_row = 0; block_row < BLOCK_SIZE; block_row++)
  {
    for (int block_col = 0; block_col < BLOCK_SIZE; block_col++)
    {
      // Iterate over cells in the current block
      int end_row_idx = (block_row+1)*BLOCK_SIZE;
      int end_col_idx = (block_col+1)*BLOCK_SIZE;

      for (int row_cursor = block_row*BLOCK_SIZE; row_cursor < end_row_idx; row_cursor++)
      {
        for (int col_cursor = block_col*BLOCK_SIZE; col_cursor < end_col_idx; col_cursor++)
        {
          // Update grid data structure
          p_sudoku_grid->grid[row_cursor][col_cursor].value = cell_values[row_cursor][col_cursor];
          p_sudoku_grid->grid[row_cursor][col_cursor].blockID.block_row = block_row;
          p_sudoku_grid->grid[row_cursor][col_cursor].blockID.block_col = block_col;
          p_sudoku_grid->grid[row_cursor][col_cursor].pBlockInfo = 
                        &p_sudoku_grid->block_info[block_row][block_col];

          if (cell_values[row_cursor][col_cursor] == EMPTY)
          {
            // Assume all numbers are candidates at the start by setting the flags to true.
            memset(p_sudoku_grid->grid[row_cursor][col_cursor].H, true, sizeof(bool)*GRID_SIZE);
            memset(p_sudoku_grid->grid[row_cursor][col_cursor].V, true, sizeof(bool)*GRID_SIZE);
            memset(p_sudoku_grid->grid[row_cursor][col_cursor].A, true, sizeof(bool)*GRID_SIZE);
            // Find the candidate numbers for the given cell at row and col. This will update 
            // the flags.
            find_candidate_numbers(row_cursor, col_cursor, cell_values, p_sudoku_grid);
          }
        } // cell col
      } // cell row
    } // block col
  } // block row

  return 0;
}

void find_candidate_numbers(int row, int col,
                            int cell_values[GRID_SIZE][GRID_SIZE],
                            tSudokuGrid *p_sudoku_grid)
{
  // Look at the row
  for (int col_cursor = 0; col_cursor < GRID_SIZE; col_cursor++)
  {
    if (cell_values[row][col_cursor] != EMPTY && col != col_cursor)
    {
      // Set flag to denote that the value at the column cursor is present and 
      // thus is not a solution candidate.
      printf("cell_values[%d][%d]: %d\n", row, col_cursor, cell_values[row][col_cursor]);
      p_sudoku_grid->grid[row][col].H[cell_values[row][col_cursor]-1] = false;
    }
  }

  // Look at the column
  for (int row_cursor = 0; row_cursor < GRID_SIZE; row_cursor++)
  {
    if (cell_values[row_cursor][col] != EMPTY && row != row_cursor)
    {
      // Set flag to denote that the value at the row cursor is present and thus
      // is not a solution candidate
      p_sudoku_grid->grid[row][col].V[cell_values[row_cursor][col]-1] = false;
    }
  }

  // Update A by doing a bitwise AND of H, V and S.
  for (int num_idx = 0; num_idx < GRID_SIZE; num_idx++)
  {
    // Set flag such that A[num_idx] is true if the number num_idx+1 is a possible candidate
    // solution.
    p_sudoku_grid->grid[row][col].A[num_idx] = p_sudoku_grid->grid[row][col].H[num_idx] &&
                                      p_sudoku_grid->grid[row][col].V[num_idx] &&
                                      p_sudoku_grid->grid[row][col].pBlockInfo->S[num_idx];
  }
}

bool has_empty_cells(tSudokuGrid *p_sudoku_grid)
{
  // Linear search for now
  // TODO: optimise later
  for (int row = 0; row < GRID_SIZE; row++)
  {
    for (int col = 0; col < GRID_SIZE; col++)
    {
      if (p_sudoku_grid->grid[row][col].value == EMPTY)
      {
        return true;
      }
    }
  }
  return false;
}

int solve_sudoku(const char *filename, tSudokuGrid *p_sudoku_grid)
{
  tBlockInfo block_info[BLOCK_SIZE][BLOCK_SIZE];

  // Attempt to solve the sudoku
  // Loop while grid contains empty cells
  while (has_empty_cells(p_sudoku_grid))
  {
    int current_iter_update_count = 0;
    
    for (int block_row = 0; block_row < BLOCK_SIZE; block_row++)
    {
      for (int block_col = 0; block_col < BLOCK_SIZE; block_col++)
      {
        printf("Processing for block (%d, %d)\n", block_row, block_col);

        // Iterate over cells in the current block
        int end_col_idx = (block_col+1)*BLOCK_SIZE;
        int end_row_idx = (block_row+1)*BLOCK_SIZE;

        for (int row_cursor = block_row*BLOCK_SIZE; row_cursor < end_row_idx; row_cursor++)
        {
          for (int col_cursor = block_col*BLOCK_SIZE; col_cursor < end_col_idx; col_cursor++)
          {
            printf("\tProcessing for cell (%d, %d)\n", row_cursor, col_cursor);
            // Only do processing if current cell in block is EMPTY
            if (p_sudoku_grid->grid[row_cursor][col_cursor].value == EMPTY)
            {
              // Does set A for the current cell have a cardinality of 1?
              int set_sum = get_set_flag_sum(p_sudoku_grid->grid[row_cursor][col_cursor].A);

              // A
              printf("\t\tset_sum: %d\n", set_sum);
              printf("\t\tA: ");
              for (int idx = 0; idx < GRID_SIZE; idx++)
              {
                printf("%d ", p_sudoku_grid->grid[row_cursor][col_cursor].A[idx]);
                if (idx == GRID_SIZE-1)
                {
                  printf("\n");
                }
              }

              // H
              printf("\t\tH: ");
              for (int idx = 0; idx < GRID_SIZE; idx++)
              {
                printf("%d ", p_sudoku_grid->grid[row_cursor][col_cursor].H[idx]);
                if (idx == GRID_SIZE-1)
                {
                  printf("\n");
                }
              }

              // V
              printf("\t\tV: ");
              for (int idx = 0; idx < GRID_SIZE; idx++)
              {
                printf("%d ", p_sudoku_grid->grid[row_cursor][col_cursor].V[idx]);
                if (idx == GRID_SIZE-1)
                {
                  printf("\n");
                }
              }

              // S
              printf("\t\tS: ");
              for (int idx = 0; idx < GRID_SIZE; idx++)
              {
                printf("%d ", p_sudoku_grid->grid[row_cursor][col_cursor].pBlockInfo->S[idx]);
                if (idx == GRID_SIZE-1)
                {
                  printf("\n");
                }
              }
              

              if (set_sum == 1)
              {
                // Find the number with the only true flag set
                int idx = 0;
                for (; idx < GRID_SIZE; idx++)
                {
                  if (p_sudoku_grid->grid[row_cursor][col_cursor].A[idx]) // only true flag set
                  {
                    p_sudoku_grid->grid[row_cursor][col_cursor].A[idx] = false;

                    p_sudoku_grid->grid[row_cursor][col_cursor].value = idx+1;
                    current_iter_update_count++;
                    // Update flags by removing this number from A, H and V of all cells
                    // which share the same column and row as this cell.
                    // Loop through all rows for a fixed column
                    int fixed_col = col_cursor;
                    for (int row = 0; row < GRID_SIZE; row++)
                    {
                      // Only if cell is not the current cell
                      if (row != row_cursor)
                      {
                        p_sudoku_grid->grid[row][fixed_col].A[idx] = false;
                        p_sudoku_grid->grid[row][fixed_col].H[idx] = false;
                        p_sudoku_grid->grid[row][fixed_col].V[idx] = false;
                      }
                    }
                    // Loop through all columns for a fixed row
                    int fixed_row = row_cursor;
                    for (int col = 0; col < GRID_SIZE; col++)
                    {
                      // Only if cell is not the current cell
                      if (col != col_cursor)
                      {
                        p_sudoku_grid->grid[fixed_row][col].A[idx] = false;
                        p_sudoku_grid->grid[fixed_row][col].H[idx] = false;
                        p_sudoku_grid->grid[fixed_row][col].V[idx] = false;
                      }
                    }
                    // Remove number from possible set (block-level)
                    p_sudoku_grid->grid[row_cursor][col_cursor].pBlockInfo->S[idx] = false;
                    break;
                  }
                }
              } // set_sum check
            }
          } // col_cursor
        } // row_cursor

        // Accumulator for bit-wise sum of all A flags in the block
        int A_block_accum[GRID_SIZE];
        memset(A_block_accum, 0, sizeof(A_block_accum));

        #if TEST == 1
        printf("Before computation, A_block_accum: ");
        for (int num_idx = 0; num_idx < GRID_SIZE; num_idx++)
        {

          printf("%d ", A_block_accum[num_idx]);
          if (num_idx == GRID_SIZE-1)
          {
            printf("\n");
          }
        }
        #endif

        // any cell in the block with only one of the A flags set but not others?
        // sum across cells in the block.
        for (int num_idx = 0; num_idx < GRID_SIZE; num_idx++)
        {
          // Iterate over cells in the current block
          int end_col_idx = (block_col+1)*BLOCK_SIZE;
          int end_row_idx = (block_row+1)*BLOCK_SIZE;

          for (int row_cursor = block_row*BLOCK_SIZE; row_cursor < end_row_idx; row_cursor++)
          {
            for (int col_cursor = block_col*BLOCK_SIZE; col_cursor < end_col_idx; col_cursor++)
            {
              if (p_sudoku_grid->grid[row_cursor][col_cursor].value == EMPTY)
              {
                // printf("p_sudoku_grid->grid[%d][%d].A[%d]: %d\n", row_cursor, col_cursor, num_idx, p_sudoku_grid->grid[row_cursor][col_cursor].A[num_idx]);
                A_block_accum[num_idx] += p_sudoku_grid->grid[row_cursor][col_cursor].A[num_idx];
              }
            }
          }
        }

        #if TEST == 1
        printf("After computation, A_block_accum: ");
        for (int num_idx = 0; num_idx < GRID_SIZE; num_idx++)
        {

          printf("%d ", A_block_accum[num_idx]);
          if (num_idx == GRID_SIZE-1)
          {
            printf("\n");
          }
        }
        #endif
      } // block_col
    } // block_row

    printf("---- Current sudoku grid: -----\n");
    for (int row_idx = 0; row_idx < GRID_SIZE; row_idx++)
    {
      for (int col_idx = 0; col_idx < GRID_SIZE; col_idx++)
      {
        if (!p_sudoku_grid->grid[row_idx][col_idx].value)
          printf("%s ", "\u25A1");
        else
          printf("%d ", p_sudoku_grid->grid[row_idx][col_idx].value);
      }
      printf("\n");
    }
    printf("-------------------------------\n");

    if (current_iter_update_count == 0)
    {
      fprintf(stderr, "Error! There should be at least one update per iteration. Is the sudoku input data valid?\n");
      return -1;
    }
  }

  // TODO: Uncomment the following once completed.
  // write_results_to_file(filename, sudoku_grid);
  return 0;  
}

int get_set_flag_sum(bool A[GRID_SIZE])
{
  int sum = 0;
  for (int idx = 0; idx < GRID_SIZE; idx++)
  {
    sum += A[idx];
  }
  return sum;
}