#include "helper.h"

#include <stdio.h>

#define TEST 1

int get_cell_values_from_file(const char *filename,
                              int grid_numbers[GRID_SIZE][GRID_SIZE])
{
  // Attempt to open the file
  FILE *pFile = NULL;
  if ((pFile = fopen(filename, "r")) == NULL)
  {
    fprintf(stderr, "Can't open file: %s\n", filename);
    return -1;
  }
  
  // If can open, read each line and store them to the grid_numbers array.
  int row = 0;
  while ((fscanf(pFile,
                "%d,%d,%d,%d,%d,%d,%d,%d,%d\n",
                &grid_numbers[row][0],
                &grid_numbers[row][1],
                &grid_numbers[row][2],
                &grid_numbers[row][3],
                &grid_numbers[row][4],
                &grid_numbers[row][5],
                &grid_numbers[row][6],
                &grid_numbers[row][7],
                &grid_numbers[row][8]) == GRID_SIZE) && 
          (row < GRID_SIZE))
  {
    row++;
  }

  fclose(pFile);

  if (row != GRID_SIZE)
  {
    fprintf(stderr, "Reading of input file not successful! Is the input file formatted correctly?\n");
    return - 1;
  }
  #if TEST == 1
  for (row = 0; row < GRID_SIZE; row++)
  {
    int col = 0;
    for (; col < GRID_SIZE; col++)
    {
      if (col == GRID_SIZE-1)
        printf("%d\n", grid_numbers[row][col]);
      else
        printf("%d,", grid_numbers[row][col]);
    }
  }
  #endif
  return 0;
}

int write_results_to_file(const char *filename,
                          tCell sudoku_grid[GRID_SIZE][GRID_SIZE])
{
  FILE *pOutputFile = NULL;
  if (fopen(filename, "w") == NULL)
  {
    return -1;
  }
  // TODO: Output results here
  fclose(pOutputFile);
}