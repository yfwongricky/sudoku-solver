#pragma once
#ifndef HELPER_H
#define HELPER_H

#include "grid.h"

int get_cell_values_from_file(const char *filename,
                               int grid_numbers[GRID_SIZE][GRID_SIZE]);

int write_results_to_file(const char *filename,
                          tCell sudoku_grid[GRID_SIZE][GRID_SIZE]);

#endif