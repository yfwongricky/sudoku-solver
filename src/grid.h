#pragma once
#ifndef GRID_H
#define GRID_H

#include <stdbool.h>

#define BLOCK_SIZE 3

#define GRID_SIZE 9

typedef enum CellValue
{
  EMPTY = 0,
  ONE,
  TWO,
  THREE,
  FOUR,
  FIVE,
  SIX,
  SEVEN,
  EIGHT,
  NINE,
} eCellValue;

typedef struct BlockID
{
  // NOTE: 0-base indices
  int block_row;
  int block_col;
} tBlockID;

typedef struct BlockInfo
{
  // Flags denoting whether the number given by 
  // array_index+1 is a possible candidate, considering
  // only the column in which the cell is located.
  bool S[GRID_SIZE];
} tBlockInfo;

typedef struct Cell
{
  // Flags denoting whether the number given by 
  // array_index+1 is a possible candidate, considering
  // only the column in which the cell is located.
  bool V[GRID_SIZE];
  // Flags denoting whether the number given by
  // array_index+1 is a possible candidate, considering
  // only the column in which the cell is located.
  bool H[GRID_SIZE];
  // Intersection of V and H
  bool A[GRID_SIZE];
  // Block to which the cell belongs
  tBlockID blockID;
  // pointer to the block metadata of which this cell is
  // part.
  tBlockInfo *pBlockInfo;
  // Cell value
  eCellValue value;
} tCell;

typedef struct SudokuGrid
{
  tCell grid[GRID_SIZE][GRID_SIZE];
  tBlockInfo block_info[BLOCK_SIZE][BLOCK_SIZE]; 
} tSudokuGrid;

// Get the block id of the given cell (row and col indices in 0-base convention)
tBlockID get_cell_block_id(int cell_row, int cell_col);

int initialize_sudoku_grid_from_file(const char *filename, tSudokuGrid *p_sudoku_grid);

// Find possible numbers for a given empty cell
void find_candidate_numbers(int row, int col,
                            int cell_values[GRID_SIZE][GRID_SIZE],
                            tSudokuGrid *p_sudoku_grid);

// Update possible numbers flags
// void update_possible_numbers_flags(cell_values[GRID_])

// Get the sum of the flags
int get_set_flag_sum(bool A[GRID_SIZE]);

// Solve sudoku and output the solution to the specified file
int solve_sudoku(const char *filename, tSudokuGrid *p_sudoku_grid);

#endif