#include "grid.h"

#include <stdio.h>
#include <string.h>

void print_usage(char *prog_name)
{
  printf("%s <input_file> <output_file>\n", prog_name);
}

int main (int argc, char *argv[])
{
  if (argc != 3)
  {
    print_usage(argv[0]);
    return 1;
  }
  tSudokuGrid SudokuGrid;
  memset(&SudokuGrid, '\0', sizeof(SudokuGrid));

  // Initialise the sudoku grid with cell values read from the file
  if (initialize_sudoku_grid_from_file(argv[1], &SudokuGrid) == -1)
  {
    return -1;
  }
  solve_sudoku(argv[2], &SudokuGrid);
  return 0;
}